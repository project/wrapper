<?php
/**
 * @file
 *   Feature set that defines some wrapper methods and examples
 */
/**
 * Implementation of hook_wrapper_item_default_wrapper().
 */
function test_wrapper_wrapper_item_default_wrapper() {
  $nodes = array();
  $nodes['Simple pages'] = array(
    'title' => 'Simple pages',
    'type' => 'wrapper',
    'source' => '!module/../example_pages/',
    'extraction_method' => 'import-xpath',
    'wrapper_settings' => array(
      'xpath' => '//body',
      'DirectoryIndex' => 'index.html',
      'content_type' => 'page',
      'recursive' => 1,
      'innerhtml' => 'InnerHTML',
      'preferred_filter' => '1',
    ),
    'update_frequency' => '0',
    'wrapper_create' => '0',
    'path' => '01-simple',
  );
  $nodes['Structured pages - Semantic divs'] = array(
    'title' => 'Structured pages - Semantic divs',
    'type' => 'wrapper',
    'source' => '!module/../example_pages/',
    'extraction_method' => 'import-xpath',
    'wrapper_settings' => array(
      'xpath' => '//div[@id="main"]',
      'DirectoryIndex' => 'index.html',
      'content_type' => 'page',
      'recursive' => 1,
      'innerhtml' => 'InnerHTML',
      'preferred_filter' => '1',
    ),
    'update_frequency' => '0',
    'wrapper_create' => '0',
    'path' => '02-structured-semantic',
  );
  $nodes['Structured pages - DOM pattern'] = array(
    'title' => 'Structured pages - DOM pattern',
    'type' => 'wrapper',
    'source' => '!module/../example_pages/',
    'extraction_method' => 'import-xpath',
    'wrapper_settings' => array(
      'xpath' => '/html/body/table/tbody/tr[3]/td[1]',
      'DirectoryIndex' => 'index.html',
      'content_type' => 'page',
      'recursive' => 1,
      'innerhtml' => 'InnerHTML',
      'preferred_filter' => '1',
    ),
    'update_frequency' => '0',
    'wrapper_create' => '0',
    'path' => '02-structured-dom',
  );
  $nodes['Structured pages - Dreamweaver tokens'] = array(
    'title' => 'Structured pages - Dreamweaver tokens',
    'type' => 'wrapper',
    'source' => '!module/../example_pages/',
    'extraction_method' => 'import-token',
    'wrapper_settings' => array(
      'DirectoryIndex' => 'index.html',
      'content_type' => 'page',
      'recursive' => 1,
      'innerhtml' => 'InnerHTML',
      'preferred_filter' => '1',
    ),
    'update_frequency' => '0',
    'wrapper_create' => '0',
    'path' => '02-structured-tokens',
  );

  return $nodes;
}
