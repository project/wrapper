<?php
/**
 * @file
 *   Additional   routines to add 'Features' exportable support to wrapper
 * items.
 *
 * A 'wrapper_item' is basically a node object of type 'wrapper'
 *
 * A 'wrapper_item' is the features 'component' that gets exported and imported.
 *
 * When serializing (or retrieving a wrapper_item via wrapper_item_load), the
 * extraneous node properties (date, ownership etc) are not retained. It's an
 * object that contains a subset of the values.
 *
 * This features extension code effectively shows a way an individual node can
 * be serialized as a feature.
 */

define('_WRAPPER_NODE_TYPE', 'wrapper');


/**
 * Implementation of hook_features_export_options().
 *
 * List all 'wrapper' type nodes currently available for export.
 *
 * The list returned is indexed by node->title, no magic machine name or id.
 *
 * @return array   A keyed array of items, suitable for use with a FormAPI
 * select or   checkboxes element.
 */
function wrapper_item_features_export_options() {
  $options = array();
  $sql = db_rewrite_sql("SELECT n.nid, n.title FROM {node} n WHERE n.type = '%s' ORDER BY n.created DESC");
  $result = db_query($sql, _WRAPPER_NODE_TYPE);
  // The 'identifier' for an item is normally its machine name.
  // I have a choice between nid, title, and something more complex.
  // I'll use title, even though it's not as tidy as a generated machine_name
  while ($node = db_fetch_object($result)) {
    $options[$node->title] = $node->title;
  }
  return $options;
}

/**
 * Implementation of hook_features_export().
 *
 * Process the export array for a given component.
 *
 * @param array $data
 *   An  array of machine names for the component in question to be exported.
 * @param array &$export
 *   By reference. An array of all components to be exported with a given
 *   feature. Component objects that should be exported should be added to
 *   this array.
 * @param string $module_name
 *   The name of the feature module to be generated.
 * @return array
 *   The pipe array of further processors that should be called.
 */
function wrapper_item_features_export($data, &$export, $module_name = '') {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['wrapper'] = 'wrapper';
  $export['dependencies']['path'] = 'path';

  foreach ($data as $identifier) {
    if ($wrapper = wrapper_item_load($identifier)) {
      $export['features']['wrapper_item'][$identifier] = $identifier;
    }
  }
  $pipe = array();
  return $pipe;
}

/**
 * Implementation of hook_features_export_render().
 *
 * Return the PHP code that represents a dump of the node items listed as $data
 */
function wrapper_item_features_export_render($module, $data) {
  $code = array();
  $code[] = "  \$nodes = array();";
  $code[] = '';

  $translatables = array();
  foreach ($data as $item_id) {
    $item = wrapper_item_load($item_id);
    if (empty($item)) {
      // Surely this should always return a valid item unless the user manually deleted the node.
      // drupal_set_message("When trying to prepare the export code, failed to retrieve the '" . _WRAPPER_NODE_TYPE . "' node called '$item_id'. Has it been deleted?");
      continue;
    }
    wrapper_item_sanitize($item, $module);
    $code[] = "  // Exported $item->type node: {$item->title}";
    $export = features_var_export($item, '  ');
    $code[] = "  \$nodes['{$item_id}'] = {$export};";
  }

  $code[] = '';
  $code[] = '  return $nodes;';
  $code = implode("\n", $code);

  return array(_WRAPPER_DEFAULT_HOOK => $code);
}


/**
 * Remove any localizations (instance-specific settings) before exporting.
 */
function wrapper_item_sanitize(&$settings, $module_name) {
  // If the path appears to be linking to the local module folder, tokenize that
  $settings->source = strtr($settings->source, array(drupal_get_path('module', $module_name) => '!module'));
}


/**
 * Implementation of hook_features_export_revert().
 */
function wrapper_item_features_revert($module) {
  wrapper_item_features_rebuild($module);
}

/**
 * Implementation of hook_features_export_rebuild().
 *
 * Create/recreate the wrapper items based on the data array.
 *
 * Data should contain a number of wrapper_item definitions. These need to be
 * created as nodes.
 *
 * If the node already exists (keyed by path) Over-write it.
 * - should it be keyed by title? We are using that just fine so far.
 *
 * Other node properties assigned to the node (CCK fileds, menu settings) are
 * not carried forward during export, and will be retained on re-import.
 */
function wrapper_item_features_rebuild($module) {
  if ($defaults = features_get_default('wrapper_item', $module)) {
    foreach ($defaults as $wrapper_id => $wrapper_item) {
      $existing = wrapper_item_load($wrapper_id, FALSE);
      if ($existing) {
        $wrapper_base = (array) $existing;
      }
      else {
        // Create a shell for the new node
        global $user;
        $wrapper_base = array(
          'type' => 'wrapper',
          'uid' => $user->uid,
        );
      }
      // Distributed features may use a local path as the source.
      // You can use the token !module in the source setting.
      // Replace that now.
      if (isset($wrapper_item['source'])) {
        $wrapper_item['source'] = strtr($wrapper_item['source'], array('!module' => drupal_get_path('module', $module)));
      }

      $wrapper_node = (object) array_merge($wrapper_base, $wrapper_item);

      // Pathauto is too aggressive, will override my desired alias unless I :
      $wrapper_node->pathauto_perform_alias = FALSE;
      node_save($wrapper_node);
      drupal_set_message(t("Rebuilt wrapper configs. !new_or_not a wrapper instance called '!link'",
        array(
        '!link' => l($wrapper_item['title'], 'node/' . $wrapper_node->nid),
        '!new_or_not' => $existing ? 'Updating' : 'Creating',
      )
      ));

      if (empty($wrapper_node->path)) {
        // Special case. If the imported wrapper node has a NULL path,
        // then this is a global wrapper, and should become a 404 handler
        // for the whole site.
        variable_set('site_404', 'node/' . $wrapper_node->nid);
        drupal_set_message(t("The wrapper node !link has a null path, therefore it is being used as a global handler for all non-existing paths within the site. Setting this sites 404 handler to use this wrapper.",
          array(
          '!link' => l($wrapper_item['title'], 'node/' . $wrapper_node->nid),
          '!new_or_not' => $existing ? 'Updating' : 'Creating',
        )
        ));
      }
    }
    // Probably should rebuild the menu about now, although node_save on a wrapper instance should do that.
  }
}


