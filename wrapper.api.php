<?php
/**
 * @file
 * Example hook implimentations for wrapper.module
 *
 *
 * You can insert actions before or after an import, when the content is getting
 * absorbed into the node.
 * 
 * @author dman dan@coders.co.nz 2011
 */
 
/** 
 * eg HOOK_wrapper_pre_parse()
 * 
 * HTML has been fetched, and a prototype $node object will soon be filled with
 * the retrieved data. 
 * Use this phase to modify the HTML string before the
 * import runs.
 * 
 * @param $html_source string
 * @param $wrapper definition of the import rules.
 * @param $node the shell node object where the new content will be put
 * 
 */
function wrapper_wrapper_pre_parse(&$html_source, &$wrapper, &$node) {
  // Remove some stupid characters that are confusing XML parser 
  // - &#13; AKA &#xD; aka a carriage return is coming out printable after XML DOM has looked at it :/ 
  $html_source = preg_replace('/\x0D/', '', $html_source);
}

/** 
 * eg HOOK_wrapper_post_parse()
 * 
 * Runs once the HTML has been parsed, and the $node values fully filled in.
 * Almost ready to render it. Use this phase for tweaks on the $node before
 * node_view() runs.
 * 
 * @param $html_source string
 * @param $wrapper definition of the import rules.
 * @param $node the shell node object where the new content will be put
 * 
 */
function wrapper_wrapper_post_parse(&$html_source, &$wrapper, &$node) {
  // If the imported content contained a form tag, it needs special attention and a different layout.
  if (preg_match('/<form/', $node->body)) {
    $node->type = 'special_node';
  }
}
 