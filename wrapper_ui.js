/** 
 * Attach an event to the wrapper method selector, 
 * to show/hide dependent elements. Via CSS.
 */
var wrapper_options = ['import-pattern', 'import-template', 'import-profile', 'import-xpath', 'import-tokens'];
if (Drupal.jsEnabled) {
  $(document).ready(function() {
    // Add the selected value to the subform class
    $('#node-form').addClass($('#edit-extraction-method').val());
    $('#edit-wrapper-extraction-method').change(
      // And update it whenever it changes
      function(e){
        for (opt in wrapper_options){ $('#node-form').removeClass(wrapper_options[opt]); }
        $('#node-form').addClass($(this).val());
      }
    )
  })
}
