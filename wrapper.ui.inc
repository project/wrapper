<?php
/**
 * @file
 * Admin widgets and forms.
 */


/**
 * Admin settings callback. A FAPI form
 */
function wrapper_admin_settings() {
  $all_wrappers = wrapper_load_all();
  $instance_links = array();
  foreach ($all_wrappers as $nid => $wrapper_node) {
    $instance_links[$nid] = array(
      'title' => $wrapper_node->title,
      'href' => "node/$nid/edit",
    );
  }

  $instance_links[] = array(
    'title' => t('New'),
    'href' => 'node/add/wrapper',
  );

  $form['instance_links'] = array(
    '#type' => 'markup',
    '#value' => theme('links', $instance_links),
  );

  $form['wrapper_show_attribution'] = array(
    '#title' => 'Display Original attribution',
    '#type' => 'checkbox',
    '#default_value' => variable_get('wrapper_show_attribution', TRUE),
    '#description' => t("
      Append a note to the page describing where and when the original was found.
    "),
  );

  $form['wrapper_use_proxy'] = array(
    '#title' => 'Use HTTP Proxy',
    '#type' => 'textfield',
    '#default_value' => variable_get('wrapper_use_proxy', ''),
    '#description' => t("
      If you are using an HTTP proxy, enter it here. E.g tcp://localhost:3434
    "),
  );

  $form['wrapper_show_debug'] = array(
    '#title' => 'Show a debug notice on wrapped pages',
    '#type' => 'checkbox',
    '#default_value' => variable_get('wrapper_show_debug', TRUE),
    '#description' => t("
      This note will only be available to users with 'view site reports' permission. It's handy during testing.
    "),
  );

  return system_settings_form($form);
}


/**
 * Some extra per-instance config options.
 * These get added to the node form.
 * * This form callback is named and chosen in wrapper_get_extraction_methods(),
 * and then invoked from wrapper_form()
 */
function wrapper_settings_subform($instance) {
  $form = array('#tree' => TRUE);

  if (! is_array($instance)) {
    $instance = array();
  }

  $instance += wrapper_default_settings();
  $form['suffixes'] = array(
    '#type' => 'textfield',
    '#title' => t('File types to rewrite'),
    '#default_value' => $instance['suffixes'],
    '#description' => t("
      Most resource file-types (binaries, unknown etc) <em>not</em> listed here
      found will be passed-through as-is.
      Enter a comma-seperated list of suffixes that <em>will</em> be parsed and      re-written.
    "),
  );

  $form['DirectoryIndex'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Document'),
    '#default_value' => $instance['DirectoryIndex'],
    '#description' => t("Enter multiple possibilities, comma-separated"),
  );

  $node_type_options = array();
  foreach (node_get_types() as $type => $typedef) {
    $node_type_options[$type] = $typedef->name;
  }
  unset($node_type_options['wrapper']);

  $form['content_type'] = array(
    '#title' => t('Node type'),
    '#type' => 'select',
    '#options' => $node_type_options,
    '#default_value' => $instance['content_type'],
    '#description' => t("When rendering wrapped pages, we pretend they are normal nodes. What node type should we say they are? This can affect theming and other display preferences."),
  );

  $form['recursive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Recursive serving'),
    '#default_value' => !empty($instance['recursive']),
    '#description' => t("
      Serve any requests under the given path with the same rules.
    "),
  );
  $form['default_mimes'] = array(
    '#type' => 'textfield',
    '#title' => t('Treat the following MIME types as if they were HTML'),
    '#default_value' => @$instance['default_mimes'],
    '#description' => t("
      Generally, wrapper will analyze and scrape only text/html documents,
      and pass though all other (assumed binary) file types unchanged.
      However, some curious servers may offer tricky MIME headers which we also have to handle,
      application/xhtml, or text/plain.
      Enter a comma-separated list here.
    "),
  );

  $form['innerhtml'] = array(
    '#type' => 'radios',
    '#title' => t('Include the surrounding pattern'),
    '#options' => array(
      'InnerHTML' => 'InnerHTML',
      'OuterHTML' => 'OuterHTML',
    ),
    '#default_value' => $instance['innerhtml'],
    '#description' => t("
      When matching a pattern, include or do not include the surrounding      delimiters.
    "),
    '#required' => TRUE,
  );

  // The input format is required, or the result will display with the 'default'
  // format, which is often brutal.
  $filter_formats = filter_formats();
  foreach ($filter_formats as $format) {
    $format_options[$format->format] = $format->name;
  }
  $form['preferred_filter'] = array(
    '#type' => 'select',
    '#title' => t('Input format (HTML filter) to run on the retrieved body copy'),
    '#options' => $format_options,
    '#default_value' => $instance['preferred_filter'],
    '#description' => t("
      If importing from a trusted, formatted source, you probably want to use      a raw or 'unfiltered' option.
    "),
  );

  $form['exec_includes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Execute PHP code in included files'),
    '#default_value' => isset($instance['exec_includes']) ? $instance['exec_includes'] : FALSE,
    '#description' => t("
      This is done by include()ing the PHP files found locally,      triggering normal evaluation.
      Most <em>local</em> legacy code and behaviour can be wrapped like this,
      although there are <a href='!help_link'>a few issues to take care of when      migrating code</a> like this.
      This is NOT the same as the PHP input filter, it happens at the server level.      Its only effective on local files.
    ", array('!help_link' => url('admin/help/wrapper'))),
  );

  // These very specific tweak options don't apply to all methods - should be moved into just xpath, token and regexp
  $form['remove_breadcrumb'] = array(
    '#title' => t('Remove breadcrumb'),
    '#type' => 'checkbox',
    '#default_value' => !empty($instance['remove_breadcrumb']),
    '#description' => t('
      Search for inline breadcrumb and removes it.
    '),
  );
  $form['remove_headings'] = array(
    '#title' => t('Remove headings'),
    '#type' => 'checkbox',
    '#default_value' => !empty($instance['remove_headings']),
    '#description' => t('
      Search for HTML headings and removes it.
    '),
  );

  $form['remove_root_headings'] = array(
    '#title' => t('Remove the heading only on the root page'),
    '#type' => 'checkbox',
    '#default_value' => !empty($instance['remove_root_headings']),
    '#description' => t('
      Search for HTML headings on the front page, but leave it on deeper pages - this can result in duplicate headings.
    '),
  );
  return $form;
}


/**
 * FAPI callback to handle wrapper_settings_subform()
 */
function wrapper_settings_subform_validate($form_id, $form_values) {
  #dpm(get_defined_vars());
}


/**
 * FAPI callback to handle wrapper_settings_subform()
 * * Save the submitted variables into a wrapper instance configurartion array.
 */
function wrapper_settings_subform_submit($form_id, $form_values) {
  $form_values['path'] = trim($form_values['path'], '/'); // no slashes

  if ($form_values['instance_id'] == 'new') {
    unset($form_values['instance_id']);
  }
  if (!$form_values['instance_id']) {
    $form_values['instance_id'] = $form_values['path'] ? str_replace('/', '-', $form_values['path']) : 'root';
  }

  $instance = $form_values;
  $instance['suffixes'] = str_replace(' ', '', $instance['suffixes']);
  $instance['filepath'] = rtrim($instance['filepath'], '/') . '/'; // ensure trailing slash

  unset($instance['op']);
  unset($instance['delete']);
  unset($instance['update']);
  unset($instance['form_token']);
  unset($instance['form_id']);

  // If the path is blank ([root]) then we need to enable the wrapper instance
  // to catch all 404s. This cannot be done through menu fallbacks.
  if ($instance['path'] == '') {
    variable_set('site_404', 'wrapper/' . $instance['instance_id'] );
    variable_set('site_frontpage', 'wrapper/' . $instance['instance_id'] );
    drupal_set_message(t('Important: As you have created a wrapper instance that takes over the whole of your Drupal site, All requests - 404s and even the home page - will now be serving content extracted from the external source. This may cause strange behaviour, and to unset it you will need to visit the site configuration settings to change the default front page and 404 handler back again.'));
  }
  #dpm(get_defined_vars());
}


/**
 * The additional settings that xpath method uses.
 *
 * @see wrapper_settings_subform()
 */
function wrapper_xpath_settings_subform($wrapper_settings) {
  $form = array();
  $form['xpath'] = array(
    '#title' => t('XPath'),
    '#type' => 'textfield',
    '#default_value' => isset($wrapper_settings['xpath']) ? $wrapper_settings['xpath'] : '//*[@id="main"]',
    '#description' => t('An XPath, eg <code>//*[@id="main"]</code>'),
    '#element_validate' => array('wrapper_xpath_settings_subform_validate'),
  );

  $form['copy_scripts'] = array(
    '#title' => t('Copy any script tags into the page as well'),
    '#type' => 'checkbox',
    '#default_value' => !empty($wrapper_settings['copy_scripts']),
    '#description' => t('
      This may be needed when importing complex wrapped items
    '),
  );

  return $form + wrapper_settings_subform($wrapper_settings);
}


/**
 * If method xpath is selected, then the pattern is required.
 * Can't make it FAPI-required as the method selector triggers this requirement.
 */
function wrapper_xpath_settings_subform_validate(&$form, &$form_state) {
  if ($form_state['values']['extraction_method'] == _WRAPPER_IMPORT_XPATH && empty($form_state['values']['wrapper_settings']['xpath'])) {
    form_set_error('wrapper_settings][xpath', t('XPath pattern is required if using the XPath method'));
  }
}


/**
 * The additional settings that tokens method uses.
 *
 * @see wrapper_settings_subform()
 */
function wrapper_tokens_settings_subform($wrapper_settings) {
  $form = array();
  $form['token_start'] = array(
    '#title' => t('Token start'),
    '#type' => 'textfield',
    '#default_value' => isset($wrapper_settings['token_start']) ? $wrapper_settings['token_start'] : '<!--START CONTENT-->',
    '#description' => t('String that marks the beginning of the content to scrape, eg <code>&lt;!-- InstanceBeginEditable name="Content" --&gt;</code>'),
  );
  $form['token_finish'] = array(
    '#title' => t('Token finish'),
    '#type' => 'textfield',
    '#default_value' => isset($wrapper_settings['token_finish']) ? $wrapper_settings['token_finish'] : '<!--FINISH CONTENT-->',
    '#description' => t('String that marks the end of the content to scrape, eg <code>&lt;!-- InstanceEndEditable --&gt;</code>'),
  );
  return $form + wrapper_settings_subform($wrapper_settings);
}


/**
 * The additional settings that import_html method uses.
 *
 * Returns  a widget for selecting an import profile from the ones provided by
 * import_html
 *
 * @see wrapper_settings_subform()
 */
function wrapper_import_html_settings_subform($wrapper_settings) {
  $import_html_profiles = variable_get('import_html_profiles', array());
  if (! empty($import_html_profiles)) {
    $profile_options = array_combine(array_keys($import_html_profiles), array_keys($import_html_profiles));
  }
  else {
    $profile_options = array();
  }
  $form = array();
  $form['profile_id'] = array(
    '#title' => t('Import profile to use'),
    '#type' => 'select',
    '#options' => $profile_options,
    '#default_value' => isset($wrapper_settings['profile_id']) ? $wrapper_settings['profile_id'] : 'default',
  );
  return $form + wrapper_settings_subform($wrapper_settings);
}


/**
 * A form widget for selecting using very simple markers
 */
function wrapper_token_selector($current_template) {
  dpm(__FUNCTION__);

  $subform = array(
    '#type' => 'markup',
    '#prefix' => "<div id='" . _WRAPPER_IMPORT_TOKENS . "-selector' class='sub-option'>",
    '#suffix' => "</div>",
    '#tree' => TRUE,
    'trim_start' => array(
      '#type' => 'textfield',
      '#title' => t('Trim Start Token'),
      '#default_value' => isset($instance[_WRAPPER_IMPORT_TOKENS]['trim_start']) ? $instance[_WRAPPER_IMPORT_TOKENS]['trim_start'] : '<body>',
      '#description' => t("If the included page needs to be stripped, what pattern indicates where to start extraction from? eg: <code>&lt;!-- BEGIN CONTENT --&gt;</code> or may be a regexp <code>&lt;body[^&gt;]*&gt;</code>"),
    ),
    'trim_start_inclusive' => array(
      '#type' => 'checkbox',
      '#title' => t('Inclusive - Include the trim marker in the result'),
      '#default_value' => $instance[_WRAPPER_IMPORT_TOKENS]['trim_start_inclusive'],
    ),
    'trim_stop' => array(
      '#type' => 'textfield',
      '#title' => t('Trim Stop Token'),
      '#default_value' => isset($instance[_WRAPPER_IMPORT_TOKENS]['trim_stop']) ? $instance[_WRAPPER_IMPORT_TOKENS]['trim_stop'] : '</body>',
      '#description' => t("Where to stop extraction. eg <code>&lt;!-- END CONTENT --&gt;</code> May be a regexp."),
    ),
    'trim_stop_inclusive' => array(
      '#type' => 'checkbox',
      '#title' => t('Inclusive - Include the trim marker in the result'),
      '#default_value' => $instance[_WRAPPER_IMPORT_TOKENS]['trim_stop_inclusive'],
    ),
  );
  return $subform;
}

/**
 * A form widget for selecting an import pattern and rules
 */
function wrapper_pattern_selector($current_template) {
  dpm(__FUNCTION__);

  return array(
    '#type' => 'textfield',
    '#title' => t('Regexp pattern to use when extracting content'),
    '#default_value' => isset($instance[_WRAPPER_IMPORT_PATTERN]) ? $instance[_WRAPPER_IMPORT_PATTERN] : "|<body[^>]*>([\s\S]*)</body>|i",
    '#prefix' => '<div id="' . _WRAPPER_IMPORT_PATTERN . '-selector" class="sub-option">',
    '#suffix' => '</div>',
    '#description' => t("Include regexp delimiters (|), and select the desired content with (). Try <pre>|&lt;body[^&gt;]*&gt;([\s\S]*)&lt;/body&gt;|i</pre>"),
  );
}


/**
 * A form widget for selecting the passthru/proxy method
 *
 * We can either fetch and serve a file, or just bounce the request.
 * Bouncing is quicker for testing, fetching is better for caching.
 */
function wrapper_passthru_selector($current_profile) {

  $options = array(
    'proxy' => 'live proxy',
    'cache' => 'caching proxy',
    'redirect' => 'redirect',
  );
  $subform = array(
    '#type' => 'markup',
    '#prefix' => "<div id='" . _WRAPPER_IMPORT_PASSTHRU . "-selector' class='sub-option'>",
    '#suffix' => "</div>",
    '#tree' => TRUE,
    'method' => array(
      '#type' => 'select',
      '#title' => t('Proxy method to use'),
      '#options' => $options,
    ),
  );
  return $subform;
}


/**
 * FAPI validation callback.
 *
 * @see wrapper_extraction_method_subform()
 */
function wrapper_extraction_method_subform_validate($form_id, $form_values) {
  // This validation may be called in a node subform OR on an instance configure form. The structure of the values is slightly different.
  $wrapper_settings = $form_values['wrapper'] ? $form_values['wrapper'] : $form_values;
  if ($wrapper_settings['extraction_method']) {
    // most of the available options are named the same as the key we need to check, so do a tricky bit of self-referential validation.
    // If there is no VALUE in the FIELD named after the METHOD, then that's a problem
    if (!$wrapper_settings[$wrapper_settings['extraction_method']]) {
      form_set_error($wrapper_settings['extraction_method'], t("To use the %method wrapper, you must choose or enter an appropriate import rule", array('%method' => $wrapper_settings['extraction_method'])));
    }
  }
}

/**
 * FAPI submit handler.
 *
 * @see wrapper_extraction_method_subform()
 */
function wrapper_extraction_method_subform_submit($form_id, $form_values) {
  dpm(__FUNCTION__);

  switch ($form_values['op']) {
    case t('Refresh now'):
      #$node = node_load($form_values['nid']);
      $node = (object) $form_values;
      if (wrapper_fetch_weblink_content($node)) {
        node_save($node);
        drupal_set_message(t('Refreshed weblink from source'));
      }
      else {
        drupal_set_message(t('Failed to refresh weblink from source'));
      }
      continue;
  }
}

